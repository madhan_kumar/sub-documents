const mongoose = require("mongoose");
const Schema = mongoose.Schema;

var nest = new Schema({
	address:{
		type:"String",		
	},
	pincode:{
		type:Number
	}
})

var user = new Schema({
	name:{
		type:"String",		
	},
	details:[nest]
})
module.exports = mongoose.model("userDetails",user);